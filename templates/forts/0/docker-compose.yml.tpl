version: '2'

services:

  forts:
    image: zelhat.net/myapp/forst:$PROVIDER_VERSION
{{- if eq .Values.LOG_ENABLE "true" }}
    volumes:
  {{- if eq .Values.LOG_HOME "true" }}  
      - /home/zelhat/{{ .Stack.Name }}/log:/app/log
  {{- else }}
      - /var/log/containers/forts-{{ .Stack.Name }}/log:/app/log
  {{- end }}
{{- end }}
    ports:
    - $PORT:8000/tcp

  logrotate:
    image: zelhat.net/myapp/logrotate:$LOGROTATE_VERSION
    volumes:
      - /home/zelhat/{{ .Stack.Name }}/log:$LOG_DIR
    environment:
      LOGS_DIRECTORIES: $LOG_DIR
      LOGROTATE_INTERVAL: $LOG_INTERVAL
      LOGROTATE_COPIES: $LOG_COPIES
      LOGROTATE_COMPRESSION: $LOG_COMPRESS
      LOGROTATE_SIZE: $LOG_SIZE
      LOGROTATE_CRONSCHEDULE: $LOG_CRONSHEDULE
    depends_on:
      - forts
